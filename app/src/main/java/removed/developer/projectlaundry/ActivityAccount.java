package removed.developer.projectlaundry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Pattern;

import removed.developer.projectlaundry.Adapter.AdapterHistoryLaundry;
import removed.developer.projectlaundry.Services.ApiClient;
import removed.developer.projectlaundry.Services.ApiService;
import removed.developer.projectlaundry.databinding.ActivityAccountBinding;
import removed.developer.projectlaundry.models.ResponseData;
import removed.developer.projectlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAccount extends AppCompatActivity implements View.OnClickListener{

    ActivityAccountBinding binding;
    private String strEmail, strName, strPhone;
    private boolean isEmailNotEmpty, isNameNotEmpty, isPhoneNotEmpty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAccountBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.buttonUpdate.setOnClickListener(this);

        getUserData(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));
    }

    @Override
    public void onClick(View view) {
        strEmail = binding.myEdittextEmail.getText().toString().trim();
        strName = binding.myEdittextName.getText().toString().trim();
        strPhone = binding.editTextPhone.getText().toString().trim();

        if (view.getId() == R.id.buttonUpdate){
            if (strEmail.equals("")){
                isEmailNotEmpty = false;
                binding.myEdittextEmail.setError("e-mail kosong");
            } else if (!isValidEmailId(strEmail)){
                isEmailNotEmpty = false;
                binding.myEdittextEmail.setError("e-mail tidak sesuai");
            } else {
                isEmailNotEmpty = true;
                strEmail = binding.myEdittextEmail.getText().toString().trim();
            }

            if (strName.equals("")){
                isNameNotEmpty = false;
                binding.myEdittextName.setError("nama kosong");
            } else {
                isNameNotEmpty = true;
                strName = binding.myEdittextName.getText().toString().trim();
            }

            if (strPhone.equals("")){
                isPhoneNotEmpty = false;
                binding.editTextPhone.setError("nomor handphone kosong");
            } else if (!isValidMobile(strPhone)){
                isPhoneNotEmpty = false;
                binding.editTextPhone.setError("nomor handphone tidak sesuai");
            } else {
                isPhoneNotEmpty = true;
                strPhone = binding.editTextPhone.getText().toString().trim();
            }

            if ((isEmailNotEmpty==true) && (isNameNotEmpty==true) && (isPhoneNotEmpty==true)){
                updateUserData(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));
            }
        }
    }

    private void getUserData(Integer id){
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getUserData(id);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body().getDataUser() != null) {
                    binding.myEdittextEmail.setText(response.body().getDataUser().getEmail());
                    binding.myEdittextName.setText(response.body().getDataUser().getName());
                    binding.editTextPhone.setText(response.body().getDataUser().getPhone());
                }else{
                    Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateUserData(Integer id){
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.updateUserData(id,
                binding.myEdittextName.getText().toString(),
                binding.myEdittextEmail.getText().toString(),
                binding.editTextPhone.getText().toString());
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                    Intent intentDashboard = new Intent(ActivityAccount.this, ActivityDashboard.class);
                    startActivity(intentDashboard);
                    finishAffinity();
                }else{
                    Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isValidEmailId(String email){
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    private boolean isValidMobile(String phone) {
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 9 && phone.length() <= 13;
        }
        return false;
    }
}