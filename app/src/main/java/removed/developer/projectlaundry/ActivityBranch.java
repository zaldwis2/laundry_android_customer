package removed.developer.projectlaundry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import removed.developer.projectlaundry.Adapter.AdapterBranchToMaps;
import removed.developer.projectlaundry.Adapter.AdapterLocationLaundry;
import removed.developer.projectlaundry.Services.ApiClient;
import removed.developer.projectlaundry.Services.ApiService;
import removed.developer.projectlaundry.databinding.ActivityBranchBinding;
import removed.developer.projectlaundry.models.ResponseData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityBranch extends AppCompatActivity {

    ActivityBranchBinding binding;
    private LinearLayoutManager linearLayoutManager;
    private AdapterBranchToMaps adapterBranchToMaps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBranchBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        showRecyclerView();

    }

    private void showRecyclerView(){
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getLaundries();
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getDataLaundries() != null) {
                        linearLayoutManager = new LinearLayoutManager(ActivityBranch.this);
                        binding.recyclerView.setLayoutManager(linearLayoutManager);
                        adapterBranchToMaps = new AdapterBranchToMaps(response.body().getDataLaundries(), getApplicationContext());
                        binding.recyclerView.setAdapter(adapterBranchToMaps);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}