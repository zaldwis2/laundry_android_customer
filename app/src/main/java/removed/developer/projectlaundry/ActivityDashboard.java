package removed.developer.projectlaundry;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import removed.developer.projectlaundry.Adapter.AdapterHistoryLaundry;
import removed.developer.projectlaundry.Adapter.AdapterLocationLaundry;
import removed.developer.projectlaundry.Adapter.AdapterLocationUser;
import removed.developer.projectlaundry.Adapter.AdapterServiceLaundry;
import removed.developer.projectlaundry.Adapter.AdapterTypePackage;
import removed.developer.projectlaundry.CustomView.MyButton;
import removed.developer.projectlaundry.DataClass.Historys;
import removed.developer.projectlaundry.DataClass.LaundryServices;
import removed.developer.projectlaundry.DataClass.LocationLaundrys;
import removed.developer.projectlaundry.DataClass.LocationUser;
import removed.developer.projectlaundry.DataClass.TypePackages;
import removed.developer.projectlaundry.Services.ApiClient;
import removed.developer.projectlaundry.Services.ApiService;
import removed.developer.projectlaundry.databinding.ActivityDashboardBinding;
import removed.developer.projectlaundry.models.DataName;
import removed.developer.projectlaundry.models.ResponseData;
import removed.developer.projectlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDashboard extends AppCompatActivity implements View.OnClickListener {

    ActivityDashboardBinding binding;
    private String greeting;
    private CardView cardViewAmountPayment, cardViewBalance, cardViewAccount, cardViewPassword, cardViewOrder, cardViewLogout;
    private TextView textViewTitlePrice, textViewBalance, textViewEmptyOrder, textViewOrderDate, textViewStatus,
            textViewFinishDate, textViewBranch, textViewPackage, textViewPrice, textViewCancel, textViewAmount, textViewPriceBill, textViewTypePackage, textViewPackagePrice, textViewTypeService, textViewServicePrice, totalPriceBill, textViewNamCustomer;
    private RecyclerView recyclerViewHistory;
    private MyButton buttonPayment;
    private AdapterLocationLaundry adapterLocationLaundry;
    private AdapterTypePackage adapterTypePackage;
    private AdapterLocationUser adapterLocationUser;
    private AdapterServiceLaundry adapterServiceLaundry;
    private AdapterHistoryLaundry adapterHistoryLaundry;
    private LinearLayoutManager linearLayoutManager;
    private LocalBroadcastManager broadcaster;
    private SwipeRefreshLayout refresh;
    private AlertDialog dialogLocationLaundry, dialogTypePackage, dialogChooseDatePickUp, dialogLocationUser, dialogLaundryService,
            dialogWaitingPickup, dialogTopupBalance;
    private String[] name;
    private String[] detailPackage;
    private String[] kostAddress;
    private String[] serviceLaundry;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.LayoutManager layoutManager2;
    RecyclerView.LayoutManager layoutManager3;
    RecyclerView.LayoutManager layoutManager4;
    int balance = 0;
    private Integer orderID = 0;
    private Integer laundryID = 0;
    private Integer packetID = 0;
    private Integer serviceID = 0;
    private Integer locationID = 0;
    private String pickupDateG = "";
    private String finishDateG = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDashboardBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        //findview layout bill
        cardViewAmountPayment = findViewById(R.id.cardViewAmountPayment);
        textViewTitlePrice = findViewById(R.id.textViewTitlePrice);
        buttonPayment = findViewById(R.id.buttonPayment);
        cardViewAccount = findViewById(R.id.cardViewAccount);
        textViewAmount = findViewById(R.id.textViewAmount);
        textViewPriceBill = findViewById(R.id.textViewPriceB);
        textViewTypePackage = findViewById(R.id.textViewTypePackageB);
        textViewPackagePrice = findViewById(R.id.textViewPackagePrice);
        textViewTypeService = findViewById(R.id.textViewTypeService);
        textViewServicePrice = findViewById(R.id.textViewServicePrice);
        totalPriceBill = findViewById(R.id.totalPriceBill);

        //findview layout akun
        textViewBalance = findViewById(R.id.textViewBalance);
        cardViewBalance = findViewById(R.id.cardViewBalance);
        recyclerViewHistory = findViewById(R.id.recyclerViewHistory);
        cardViewPassword = findViewById(R.id.cardViewPassword);
        cardViewLogout = findViewById(R.id.cardView6);
        textViewNamCustomer = findViewById(R.id.textViewNamCustomer);

        //findview layout order
        cardViewOrder = findViewById(R.id.cardViewOrder);
        textViewEmptyOrder = findViewById(R.id.textViewEmptyOrder);
        textViewOrderDate = findViewById(R.id.textViewOrderDate);
        textViewStatus = findViewById(R.id.textViewStatus);
        textViewFinishDate = findViewById(R.id.textViewFinishDate);
        textViewBranch = findViewById(R.id.textViewBranch);
        textViewPackage = findViewById(R.id.textViewPackage);
        textViewPrice = findViewById(R.id.textViewPrice);
        textViewCancel = findViewById(R.id.textViewCancel);

        binding.textViewLaundryLocation.setOnClickListener(this);
        binding.textViewTypePackage.setOnClickListener(this);
        binding.textViewDatePickup.setOnClickListener(this);
        binding.textViewLocationUser.setOnClickListener(this);
        binding.textViewChooseService.setOnClickListener(this);
        binding.cardView2.setOnClickListener(this);
        binding.buttonOrder.setOnClickListener(this);
        cardViewBalance.setOnClickListener(this);
        cardViewAccount.setOnClickListener(this);
        cardViewPassword.setOnClickListener(this);
        cardViewLogout.setOnClickListener(this);
        textViewCancel.setOnClickListener(this);
        buttonPayment.setOnClickListener(this);
        broadcaster = LocalBroadcastManager.getInstance(this);
        hideBill();
        setGreeting();
        refreshHistory();
        getOrders(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));
        getUserData(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));

        binding.textViewGreeting.setText(greeting);
        getBalance(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));


    }

    private void refreshHistory() {
        binding.refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrder(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));
                getOrders(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.refresh.setRefreshing(false);
                    }
                }, 1000);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
                new IntentFilter("chooseLocation"));
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver2),
                new IntentFilter("choosePackage"));
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver3),
                new IntentFilter("chooseLocationUser"));
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver4),
                new IntentFilter("chooseServiceLaundry"));
        getOrder(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver2);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver3);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver4);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ((!(intent.getStringExtra("laundry_name") == null))) {
                binding.textViewLaundryLocation.setText(intent.getStringExtra("laundry_name"));
                laundryID = intent.getIntExtra("laundry_id", 0);
                dialogLocationLaundry.dismiss();
            }
        }
    };

    private BroadcastReceiver mMessageReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ((!(intent.getStringExtra("packet_name") == null))) {
                binding.textViewTypePackage.setText(intent.getStringExtra("packet_name"));
                packetID = intent.getIntExtra("packet_id", 0);
                dialogTypePackage.dismiss();
                setDateEndService();
            }
        }
    };

    private BroadcastReceiver mMessageReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ((!(intent.getStringExtra("location_name") == null))) {
                binding.textViewLocationUser.setText(intent.getStringExtra("location_name"));
                locationID = intent.getIntExtra("location_id", 0);
                dialogLocationUser.dismiss();
            }
        }
    };

    private BroadcastReceiver mMessageReceiver4 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ((!(intent.getStringExtra("service_name") == null))) {
                binding.textViewChooseService.setText(intent.getStringExtra("service_name"));
                serviceID = intent.getIntExtra("service_id", 0);
                dialogLaundryService.dismiss();
                binding.buttonOrder.setEnabled(true);
            }
        }
    };

    private void setDateEndService() {
        String startDate = binding.textViewDatePickup.getText().toString();
        if (packetID == 1) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(startDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.add(Calendar.DAY_OF_MONTH, 1);
            String finishDate = sdf.format(c.getTime());
            binding.textViewEnd.setText(finishDate);
            finishDateG = finishDate;
        } else if (packetID == 2) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(startDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.add(Calendar.DAY_OF_MONTH, 3);
            String finishDate = sdf.format(c.getTime());
            binding.textViewEnd.setText(finishDate);
            finishDateG = finishDate;
        } else {
            binding.textViewEnd.setText("-");
        }
    }

    private void setGreeting() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int hour = cal.get(Calendar.HOUR_OF_DAY);

        if (hour >= 12 && hour < 17) {
            greeting = "Selamat Siang, Kak";
        } else if (hour >= 17 && hour < 21) {
            greeting = "Selamat Sore, Kak";
        } else if (hour >= 21 && hour < 24) {
            greeting = "Selamat Malam, Kak";
        } else {
            greeting = "Selamat Pagi, Kak";
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textViewLaundryLocation:
                getLaundries();
                break;

            case R.id.textViewTypePackage:
                if (binding.textViewDatePickup.getText().toString().equals("Pilih Tanggal")) {
                    Toast.makeText(this, "pilih tanggal jemputnya dulu kak", Toast.LENGTH_SHORT).show();
                } else {
                    getPackets();
                }
                break;

            case R.id.textViewDatePickup:
                if (binding.textViewLaundryLocation.getText().toString().equals("Pilih Lokasi")) {
                    Toast.makeText(this, "pilih lokasi laundry nya dulu kak", Toast.LENGTH_SHORT).show();
                } else {
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.YEAR, 12);

                    AlertDialog.Builder builderChooseDatePickUp = new AlertDialog.Builder(this);
                    View dialogChooseDate = getLayoutInflater().inflate(R.layout.dialog_date_picker, null);
                    builderChooseDatePickUp.setCancelable(true);
                    builderChooseDatePickUp.setView(dialogChooseDate);
                    dialogChooseDatePickUp = builderChooseDatePickUp.create();

                    final DatePicker datePickup = dialogChooseDate.findViewById(R.id.datepicker);
                    datePickup.setMinDate(System.currentTimeMillis() - 1000);
                    TextView cancelBtn = dialogChooseDate.findViewById(R.id.datepicker_cancel_btn);
                    TextView okBtn = dialogChooseDate.findViewById(R.id.datepicker_ok_btn);

                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogChooseDatePickUp.dismiss();
                        }
                    });

                    okBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String pickUp = datePickup.getDayOfMonth() + "-" + (datePickup.getMonth() + 1) + "-" + datePickup.getYear();
                            binding.textViewDatePickup.setText(pickUp);
                            pickupDateG = pickUp;
                            dialogChooseDatePickUp.dismiss();
                        }
                    });
                    dialogChooseDatePickUp.show();
                }
                break;

            case R.id.textViewChooseService:
                if (binding.textViewLocationUser.getText().toString().equals("Pilih Lokasi")) {
                    Toast.makeText(this, "kasih tau lokasi kakak dulu", Toast.LENGTH_SHORT).show();
                } else {
                    getServices();
                }
                break;

            case R.id.textViewLocationUser:
                if (binding.textViewTypePackage.getText().toString().equals("Pilih Paket")) {
                    Toast.makeText(this, "pilih paketnya dulu kak", Toast.LENGTH_SHORT).show();
                    binding.buttonOrder.setEnabled(false);
                } else {
                    getLocations();
                }
                break;

            case R.id.buttonOrder:
                setDialogWaitingPickup();
                placeOrder(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()), locationID, laundryID, serviceID, packetID, pickupDateG, finishDateG);
                break;

            case R.id.cardViewBalance:
                setDialogTopup();
                break;

            case R.id.cardViewAccount:
                Intent intentAccount = new Intent(ActivityDashboard.this, ActivityAccount.class);
                startActivity(intentAccount);
                break;

            case R.id.cardViewPassword:
                Intent intentPassword = new Intent(ActivityDashboard.this, ActivityPassword.class);
                startActivity(intentPassword);
                break;

            case R.id.cardView6:
                SharedPreferenceManager.getInstance(ActivityDashboard.this).setUserId("");
                Intent intent = new Intent(ActivityDashboard.this, ActivityLogin.class);
                startActivity(intent);
                finishAffinity();
                break;

            case R.id.textViewCancel:
                binding.buttonOrder.setEnabled(false);
                cancelOrder(orderID);
                break;

            case R.id.buttonPayment:
                payOrder(orderID, Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));
                break;

            case R.id.cardView2:
                Intent intentBranch = new Intent(ActivityDashboard.this, ActivityBranch.class);
                startActivity(intentBranch);
                break;
        }
    }

    private void getUserData(Integer id) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getUserData(id);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body().getDataUser() != null) {
                    textViewNamCustomer.setText(response.body().getDataUser().getName());
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setDialogWaitingPickup() {
        AlertDialog.Builder builderWaiting = new AlertDialog.Builder(this);
        View dialogWaiting = getLayoutInflater().inflate(R.layout.popup_waiting_pickup, null);
        builderWaiting.setCancelable(false);
        builderWaiting.setView(dialogWaiting);
        dialogWaitingPickup = builderWaiting.create();
        dialogWaitingPickup.show();
        Window window = dialogWaitingPickup.getWindow();
        window.setLayout(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
    }

    private void setDialogTopup() {
        AlertDialog.Builder builderTopup = new AlertDialog.Builder(this);
        View dialogTopup = getLayoutInflater().inflate(R.layout.popup_topup, null);
        builderTopup.setCancelable(true);
        builderTopup.setView(dialogTopup);
        Button buttonTopup = dialogTopup.findViewById(R.id.buttonTopup);
        final EditText editTextBalance = dialogTopup.findViewById(R.id.editTextBalance);
        dialogTopupBalance = builderTopup.create();
        dialogTopupBalance.show();
        Window window = dialogTopupBalance.getWindow();
        window.setLayout(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        buttonTopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!editTextBalance.getText().toString().isEmpty()) {
                    balance = Integer.parseInt(editTextBalance.getText().toString().trim());
                    updateBalance(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()), balance);
                } else {
                    dialogTopupBalance.dismiss();
                }
            }
        });
    }

    private void hideBill() {
        cardViewAmountPayment.setVisibility(View.GONE);
        textViewTitlePrice.setVisibility(View.GONE);
        buttonPayment.setVisibility(View.GONE);
    }

    private void showOrder(String pickupDate, String finishDate, String laundry, String packet, String price, String status) {
        textViewOrderDate.setText(pickupDate);
        textViewFinishDate.setText(finishDate);
        textViewBranch.setText(laundry);
        textViewPackage.setText(packet);
        textViewPrice.setText(price);
        textViewStatus.setText(status);
        textViewEmptyOrder.setVisibility(View.GONE);
        cardViewOrder.setVisibility(View.VISIBLE);

        if (textViewStatus.getText().toString().trim().equals("Sedang Dicuci")){
            activeOrder();
            hideOrder();
        } else {
            unActiveOrder();
        }
    }

    private void showBill() {
        cardViewAmountPayment.setVisibility(View.VISIBLE);
        textViewTitlePrice.setVisibility(View.VISIBLE);
        buttonPayment.setVisibility(View.VISIBLE);
    }

    private void unActiveOrder() {
        binding.textViewLaundryLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ActivityDashboard.this, "orderan sebelumnya lagi diproses kak, mohon tunggu ya", Toast.LENGTH_SHORT).show();
            }
        });
        binding.textViewLaundryLocation.setText("Pilih Lokasi");
        binding.textViewDatePickup.setText("Pilih Tanggal");
        binding.textViewTypePackage.setText("Pilih Paket");
        binding.textViewLocationUser.setText("Pilih Lokasi");
        binding.textViewChooseService.setText("Pilih Layanan");
        binding.textViewEnd.setText("-");
    }

    private void activeOrder() {
        binding.textViewLaundryLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLaundries();
            }
        });
    }

    private void hideOrder() {
        textViewEmptyOrder.setVisibility(View.VISIBLE);
        cardViewOrder.setVisibility(View.GONE);
    }

    private void getLaundries() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View dialogLocation = getLayoutInflater().inflate(R.layout.popup_location, null);
        builder.setCancelable(true);
        builder.setView(dialogLocation);
        RecyclerView recyclerView = dialogLocation.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        dialogLocationLaundry = builder.create();
        dialogLocationLaundry.show();
        Window window = dialogLocationLaundry.getWindow();
        window.setLayout(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getLaundries();
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getDataLaundries() != null) {
                        linearLayoutManager = new LinearLayoutManager(ActivityDashboard.this);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        adapterLocationLaundry = new AdapterLocationLaundry(response.body().getDataLaundries(), getApplicationContext());
                        recyclerView.setAdapter(adapterLocationLaundry);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getPackets() {
        AlertDialog.Builder builderTypePackage = new AlertDialog.Builder(this);
        View dialogPackage = getLayoutInflater().inflate(R.layout.popup_package, null);
        builderTypePackage.setCancelable(true);
        builderTypePackage.setView(dialogPackage);
        RecyclerView recyclerViewPackage = dialogPackage.findViewById(R.id.recyclerView);
        TextView textViewTitle = dialogPackage.findViewById(R.id.textViewTitle);
        dialogTypePackage = builderTypePackage.create();
        dialogTypePackage.show();
        Window windowPackage = dialogTypePackage.getWindow();
        windowPackage.setLayout(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        textViewTitle.setText("Pilih Paket :");

        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getPackets();
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getDataPackets() != null) {
                        linearLayoutManager = new LinearLayoutManager(ActivityDashboard.this);
                        recyclerViewPackage.setLayoutManager(linearLayoutManager);
                        adapterTypePackage = new AdapterTypePackage(response.body().getDataPackets(), getApplicationContext());
                        recyclerViewPackage.setAdapter(adapterTypePackage);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getLocations() {
        AlertDialog.Builder builderKostUser = new AlertDialog.Builder(this);
        View dialogKostAddress = getLayoutInflater().inflate(R.layout.popup_location, null);
        builderKostUser.setCancelable(true);
        builderKostUser.setView(dialogKostAddress);
        RecyclerView recyclerViewKostUser = dialogKostAddress.findViewById(R.id.recyclerView);
        TextView textViewTitle = dialogKostAddress.findViewById(R.id.textViewTitle);
        recyclerViewKostUser.setHasFixedSize(true);
        dialogLocationUser = builderKostUser.create();
        dialogLocationUser.show();
        Window windowKost = dialogLocationUser.getWindow();
        windowKost.setLayout(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        textViewTitle.setText("Lokasi Kost Kamu :");

        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getLocations();
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getDataLocations() != null) {
                        linearLayoutManager = new LinearLayoutManager(ActivityDashboard.this);
                        recyclerViewKostUser.setLayoutManager(linearLayoutManager);
                        adapterLocationUser = new AdapterLocationUser(response.body().getDataLocations(), getApplicationContext());
                        recyclerViewKostUser.setAdapter(adapterLocationUser);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getServices() {
        AlertDialog.Builder builderLaundryService = new AlertDialog.Builder(this);
        View dialogService = getLayoutInflater().inflate(R.layout.popup_location, null);
        builderLaundryService.setCancelable(true);
        builderLaundryService.setView(dialogService);
        RecyclerView recyclerViewLaundryService = dialogService.findViewById(R.id.recyclerView);
        TextView textViewTitleService = dialogService.findViewById(R.id.textViewTitle);
        recyclerViewLaundryService.setHasFixedSize(true);
        dialogLaundryService = builderLaundryService.create();
        dialogLaundryService.show();
        Window windowService = dialogLaundryService.getWindow();
        windowService.setLayout(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        textViewTitleService.setText("Pilih pelayanan nya kak :");

        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getServices();
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getDataServices() != null) {
                        linearLayoutManager = new LinearLayoutManager(ActivityDashboard.this);
                        recyclerViewLaundryService.setLayoutManager(linearLayoutManager);
                        adapterServiceLaundry = new AdapterServiceLaundry(response.body().getDataServices(), getApplicationContext());
                        recyclerViewLaundryService.setAdapter(adapterServiceLaundry);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void placeOrder(Integer customer_id, Integer location_id, Integer laundry_id, Integer service_id, Integer packet_id, String pickup, String finish) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.order(customer_id, location_id, laundry_id, service_id, packet_id, pickup, finish);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()) {
                    dialogWaitingPickup.dismiss();
                    getOrder(customer_id);
                    binding.buttonOrder.setEnabled(false);
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getOrder(Integer customer_id) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getOrder(customer_id);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body().getDataOrder() != null) {
                    orderID = response.body().getDataOrder().getId();
                    String status = "";
                    if (response.body().getDataOrder().getIsDelivered() == 1) {
                        status = "Sudah Selesai";
                    } else if (response.body().getDataOrder().getIsProcessed() == 1) {
                        status = "Sedang Dicuci";
                        textViewCancel.setVisibility(View.GONE);
                        hideOrder();
                    } else if (response.body().getDataOrder().getIsPicked() == 1) {
                        status = "Sudah Diambil";
                        textViewCancel.setVisibility(View.GONE);
                    } else if (response.body().getDataOrder().getIsAccepted() == 1) {
                        status = "Menunggu Jemput";
                    } else {
                        status = "Menunggu Respon";
                    }

                    if (response.body().getDataOrder().getIsDelivered() != 1) {
                        showOrder(response.body().getDataOrder().getPickupTime(),
                                response.body().getDataOrder().getFinishTime(),
                                response.body().getDataOrder().getLaundryName(),
                                response.body().getDataOrder().getPacketName(),
                                String.valueOf(response.body().getDataOrder().getPrice()),
                                status
                        );
//                        unActiveOrder();
                    }

                    if (response.body().getDataOrder().getIsPaid() == 0 && response.body().getDataOrder().getIsPicked() == 1) {
                        Integer unit_price = 0;
                        unit_price = response.body().getDataOrder().getWeight() * 7000;
                        Log.d("unit price", "onResponse: " + unit_price);

                        textViewAmount.setText(response.body().getDataOrder().getWeight() + " kg");
                        textViewPriceBill.setText("Rp. " + unit_price);
                        textViewTypePackage.setText(response.body().getDataOrder().getPacketName());
                        if (response.body().getDataOrder().getPacketId() == 1) {
                            textViewPackagePrice.setText("Rp. 5000");
                        } else {
                            textViewPackagePrice.setText("Rp. 0");
                        }
                        if (response.body().getDataOrder().getServiceId() == 1) {
                            textViewTypeService.setText("Cuci + Gosok");
                            textViewServicePrice.setText("Rp. 5000");
                        } else {
                            textViewTypeService.setText("Cuci + Lipat");
                            textViewServicePrice.setText("Rp. 0");
                        }
                        totalPriceBill.setText("Rp. " + response.body().getDataOrder().getPrice());
                        showBill();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void cancelOrder(Integer orderID) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.cancelOrder(orderID, 1, 1, 1, 1);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()) {
                    activeOrder();
                    hideOrder();
                    hideBill();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getOrders(Integer customer_id) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getOrders(customer_id);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body().getDataOrders() != null) {
                    layoutManager4 = new LinearLayoutManager(getApplicationContext());
                    recyclerViewHistory.setLayoutManager(layoutManager4);
                    recyclerViewHistory.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, true));
                    adapterHistoryLaundry = new AdapterHistoryLaundry(response.body().getDataOrders(), getApplicationContext());
                    recyclerViewHistory.setAdapter(adapterHistoryLaundry);
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateBalance(Integer id, Integer balance) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.updateBalance(id, balance);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body().getBalance() != null) {
                    Locale localeID = new Locale("in", "ID");
                    final NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                    textViewBalance.setText(formatRupiah.format((double) response.body().getBalance()));
                    dialogTopupBalance.dismiss();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getBalance(Integer id) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.getBalance(id);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body().getBalance() != null) {
                    Locale localeID = new Locale("in", "ID");
                    final NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                    textViewBalance.setText(formatRupiah.format((double) response.body().getBalance()));
                    balance = response.body().getBalance();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void payOrder(Integer orderID, Integer customerID) {
        buttonPayment.setEnabled(false);
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.payOrder(orderID, customerID);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Locale localeID = new Locale("in", "ID");
                    final NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                    textViewBalance.setText(formatRupiah.format((double) response.body().getBalance()));

                    hideBill();
                    getOrder(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));
                    getOrders(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));
                } else {
                    Toast.makeText(getApplicationContext(), "Isi saldonya dulu kak", Toast.LENGTH_LONG).show();
                    buttonPayment.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                buttonPayment.setEnabled(true);
            }
        });
    }
}