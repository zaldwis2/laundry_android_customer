package removed.developer.projectlaundry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Pattern;

import removed.developer.projectlaundry.Services.ApiClient;
import removed.developer.projectlaundry.Services.ApiService;
import removed.developer.projectlaundry.databinding.ActivityForgotPasswordBinding;
import removed.developer.projectlaundry.models.ResponseData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityForgotPassword extends AppCompatActivity implements View.OnClickListener{

    ActivityForgotPasswordBinding binding;
    private String strEmail, strName, strPhone;
    private boolean isEmailNotEmpty, isNameNotEmpty, isPhoneNotEmpty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityForgotPasswordBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.buttonReset.setOnClickListener(this);

        binding.toolbar.setTitle("Lupa Password");
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View view) {
        strEmail = binding.myEdittextEmail.getText().toString().trim();
        strName = binding.myEdittextName.getText().toString().trim();
        strPhone = binding.editTextPhone.getText().toString().trim();

        if (view.getId()==R.id.buttonReset){
            if (strEmail.equals("")){
                isEmailNotEmpty = false;
                binding.myEdittextEmail.setError("e-mail kosong");
            } else if (!isValidEmailId(strEmail)){
                isEmailNotEmpty = false;
                binding.myEdittextEmail.setError("e-mail tidak sesuai");
            } else {
                isEmailNotEmpty = true;
                strEmail = binding.myEdittextEmail.getText().toString().trim();
            }

            if (strName.equals("")){
                isNameNotEmpty = false;
                binding.myEdittextName.setError("nama kosong");
            } else {
                isNameNotEmpty = true;
                strName = binding.myEdittextName.getText().toString().trim();
            }

            if (strPhone.equals("")){
                isPhoneNotEmpty = false;
                binding.editTextPhone.setError("nomor handphone kosong");
            } else if (!isValidMobile(strPhone)){
                isPhoneNotEmpty = false;
                binding.editTextPhone.setError("nomor handphone tidak sesuai");
            } else {
                isPhoneNotEmpty = true;
                strPhone = binding.editTextPhone.getText().toString().trim();
            }

            if ((isEmailNotEmpty==true) && (isNameNotEmpty==true) && (isPhoneNotEmpty==true)){
                forgotPasswordAPI();
            }
        }
    }

    private void forgotPasswordAPI() {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.forgotPassword(
                binding.myEdittextEmail.getText().toString(),
                binding.myEdittextName.getText().toString(),
                binding.editTextPhone.getText().toString());
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() !=null) {
                    Intent intentReset = new Intent(ActivityForgotPassword.this, ActivityResetPassword.class);
                    intentReset.putExtra("user_id",response.body().getDataUser().getId());
                    startActivity(intentReset);
                }else{
                    Toast.makeText(getApplicationContext(),"Incorrect Data",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isValidEmailId(String email){
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    private boolean isValidMobile(String phone) {
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 9 && phone.length() <= 13;
        }
        return false;
    }
}