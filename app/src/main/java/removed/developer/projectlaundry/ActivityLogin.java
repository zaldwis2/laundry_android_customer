package removed.developer.projectlaundry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Pattern;

import removed.developer.projectlaundry.Services.ApiClient;
import removed.developer.projectlaundry.Services.ApiService;
import removed.developer.projectlaundry.databinding.ActivityLoginBinding;
import removed.developer.projectlaundry.models.ResponseData;
import removed.developer.projectlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener{

    ActivityLoginBinding binding;
    private String strEmail, strPassword;
    private boolean isEmailNotEmpty, isPasswordNotEmpty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.buttonLogin.setOnClickListener(this);
        binding.textViewRegister.setOnClickListener(this);
        binding.textViewForgotPassword.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        strEmail = binding.editTextEmail.getText().toString().trim();
        strPassword = binding.editTextPassword.getText().toString().trim();
        switch (view.getId()){
            case R.id.textViewRegister:
                Intent intentRegister = new Intent(ActivityLogin.this, ActivityRegister.class);
                startActivity(intentRegister);
                break;

            case R.id.buttonLogin:
                if (strEmail.equals("")){
                    isEmailNotEmpty = false;
                    binding.editTextEmail.setError("e-mail kosong");
                } else if (!isValidEmailId(strEmail)){
                    isEmailNotEmpty = false;
                    binding.editTextEmail.setError("e-mail tidak sesuai");
                } else {
                    strEmail = binding.editTextEmail.getText().toString().trim();
                    isEmailNotEmpty = true;
                }

                if (strPassword.equals("")){
                    isPasswordNotEmpty = false;
                    binding.editTextPassword.setError("password kosong");
                } else {
                    isPasswordNotEmpty =true;
                    strPassword = binding.editTextPassword.getText().toString().trim();
                    if ((isEmailNotEmpty==true) && (isPasswordNotEmpty==true)){
                        loginAPI();
                    }
                }
                break;

            case R.id.textViewForgotPassword:
                Intent intentForgot = new Intent(ActivityLogin.this, ActivityForgotPassword.class);
                startActivity(intentForgot);
                break;
        }
    }

    private void loginAPI() {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.login(
                binding.editTextEmail.getText().toString(),
                binding.editTextPassword.getText().toString());
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() !=null) {
                    SharedPreferenceManager.getInstance(ActivityLogin.this).setUserId(String.valueOf(response.body().getDataUser().getId()));
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(ActivityLogin.this, ActivityDashboard.class);
                    startActivity(intent);
                    finishAffinity();
                }else{
                    Toast.makeText(getApplicationContext(),"Wrong Password or Email",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isValidEmailId(String email){
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }
}