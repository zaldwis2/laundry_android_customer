package removed.developer.projectlaundry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import removed.developer.projectlaundry.Adapter.AdapterHistoryLaundry;
import removed.developer.projectlaundry.Services.ApiClient;
import removed.developer.projectlaundry.Services.ApiService;
import removed.developer.projectlaundry.databinding.ActivityPasswordBinding;
import removed.developer.projectlaundry.models.ResponseData;
import removed.developer.projectlaundry.utility.SharedPreferenceManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPassword extends AppCompatActivity {

    ActivityPasswordBinding binding;
    private String strOldPassword, strNewPassword, strConfirmNewPassword;
    private boolean oldPassFill, newPassFill, confirmNewPassFill = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPasswordBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOldPassword = binding.myEdittextOldPassword.getText().toString().trim();
                strNewPassword = binding.myEdittextNewPassword.getText().toString().trim();
                strConfirmNewPassword = binding.editTextConfirmPassword.getText().toString().trim();

                if (strOldPassword.equals("")) {
                    oldPassFill = false;
                    binding.myEdittextOldPassword.setError("password lama kosong");
                } else {
                    oldPassFill = true;
                    strOldPassword = binding.myEdittextOldPassword.getText().toString().trim();
                }

                if (strNewPassword.equals("")) {
                    newPassFill = false;
                    binding.myEdittextNewPassword.setError("password baru kosong");
                } else {
                    newPassFill = true;
                    strNewPassword = binding.myEdittextNewPassword.getText().toString().trim();
                }

                if (strConfirmNewPassword.equals("")) {
                    confirmNewPassFill = false;
                    binding.editTextConfirmPassword.setError("konfirmasi password kosong");
                } else if (!strConfirmNewPassword.equals(strNewPassword)) {
                    confirmNewPassFill = false;
                    binding.editTextConfirmPassword.setError("konfirmasi password tidak sesuai");
                } else {
                    confirmNewPassFill = true;
                    strConfirmNewPassword = binding.editTextConfirmPassword.getText().toString().trim();
                }

                if ((oldPassFill == true) && (newPassFill == true) && (confirmNewPassFill == true)) {
                    changePassword(Integer.valueOf(SharedPreferenceManager.getInstance(getApplicationContext()).getUserId()));
                }
            }
        });
    }

    private void changePassword(Integer id) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.changePassword2(id,
                binding.myEdittextOldPassword.getText().toString(),
                binding.editTextConfirmPassword.getText().toString());
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}