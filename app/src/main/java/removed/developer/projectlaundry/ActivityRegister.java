package removed.developer.projectlaundry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.regex.Pattern;

import removed.developer.projectlaundry.Services.ApiClient;
import removed.developer.projectlaundry.Services.ApiService;
import removed.developer.projectlaundry.databinding.ActivityRegisterBinding;
import removed.developer.projectlaundry.models.ResponseData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityRegister extends AppCompatActivity implements View.OnClickListener{

    ActivityRegisterBinding binding;
    private String strEmail, strName, strPhone, strPassword, strConfirmPassword;
    private boolean emailFill, nameFill, phoneFill, passwordFill, confirmPassFill = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.textViewLogin.setOnClickListener(this);
        binding.buttonRegister.setOnClickListener(this);

    }

    private void registerAPI() {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.register(
                binding.editTextEmail.getText().toString(),
                binding.editTextName.getText().toString(),
                binding.editTextPhone.getText().toString(),
                binding.editTextConfirmPassword.getText().toString());
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() !=null) {
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(ActivityRegister.this, ActivityLogin.class);
                    startActivity(intent);
                    finishAffinity();
                }else{
                    Toast.makeText(getApplicationContext(),"Email Already Exist",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        strEmail = binding.editTextEmail.getText().toString().trim();
        strName = binding.editTextName.getText().toString().trim();
        strPhone = binding.editTextPhone.getText().toString().trim();
        strPassword = binding.editTextPassword.getText().toString().trim();
        strConfirmPassword = binding.editTextConfirmPassword.getText().toString().trim();

        switch (view.getId()){
            case R.id.textViewLogin:
                onBackPressed();
                break;

            case R.id.buttonRegister:

                if (strEmail.equals("")){
                    emailFill = false;
                    binding.editTextEmail.setError("e-mail kosong");
                } else if (!isValidEmailId(strEmail)){
                    emailFill = false;
                    binding.editTextEmail.setError("e-mail tidak sesuai");
                } else {
                    emailFill = true;
                    strEmail = binding.editTextEmail.getText().toString().trim();
                }

                if (strName.equals("")){
                    nameFill = false;
                    binding.editTextName.setError("nama kosong");
                } else {
                    nameFill = true;
                    strName = binding.editTextName.getText().toString().trim();
                }

                if (strPhone.equals("")){
                    phoneFill = false;
                    binding.editTextPhone.setError("nomor hp kosong");
                } else if (!isValidMobile(strPhone)){
                    phoneFill = false;
                    binding.editTextPhone.setError("nomor hp tidak sesuai");
                } else {
                    phoneFill = true;
                    strPhone = binding.editTextPhone.getText().toString().trim();
                }

                if (strPassword.equals("")){
                    passwordFill = false;
                    binding.editTextPassword.setError("password kosong");
                } else {
                    passwordFill = true;
                    strPassword = binding.editTextPassword.getText().toString().trim();
                }

                if (strConfirmPassword.equals("")){
                    confirmPassFill = false;
                    binding.editTextConfirmPassword.setError("konfirmasi password kosong");
                } else if (!strConfirmPassword.equals(strPassword)){
                    confirmPassFill = false;
                    binding.editTextConfirmPassword.setError("konfirmasi password tidak sesuai");
                } else {
                    confirmPassFill = true;
                    strConfirmPassword = binding.editTextConfirmPassword.getText().toString().trim();
                }

                if ((emailFill==true) && (nameFill==true) && (phoneFill==true) && (passwordFill==true) && (confirmPassFill==true)){
                    registerAPI();
                }

                break;
        }

    }

    private boolean isValidEmailId(String email){
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    private boolean isValidMobile(String phone) {
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 9 && phone.length() <= 13;
        }
        return false;
    }
}