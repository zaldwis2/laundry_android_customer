package removed.developer.projectlaundry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import removed.developer.projectlaundry.Services.ApiClient;
import removed.developer.projectlaundry.Services.ApiService;
import removed.developer.projectlaundry.databinding.ActivityResetPasswordBinding;
import removed.developer.projectlaundry.models.ResponseData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityResetPassword extends AppCompatActivity implements View.OnClickListener{
    private Integer userID = 0;

    ActivityResetPasswordBinding binding;
    private String strNewPassword, strConfirmNewPassword;
    private boolean isNewPasswordNotEmpty, isConfirmPasswordNotEmpty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityResetPasswordBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        Intent intent =getIntent();

        if (intent.getIntExtra("user_id",0)!=0){
            userID = intent.getIntExtra("user_id",0);
        }

        binding.buttonChangePassword.setOnClickListener(this);

        binding.toolbar.setTitle("Reset Password");
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        strNewPassword = binding.editTextNewPassword.getText().toString().trim();
        strConfirmNewPassword = binding.editTextConfirmPassword.getText().toString().trim();

        if (view.getId()==R.id.buttonChangePassword){
            if (strNewPassword.equals("")){
                isNewPasswordNotEmpty = false;
                binding.editTextNewPassword.setError("password baru kosong");
            } else {
                isNewPasswordNotEmpty = true;
                strNewPassword = binding.editTextNewPassword.getText().toString().trim();
            }

            if (strConfirmNewPassword.equals("")){
                isConfirmPasswordNotEmpty = false;
                binding.editTextConfirmPassword.setError("konfirmasi password baru kosong");
            } else if (!strConfirmNewPassword.equals(strNewPassword)){
                isConfirmPasswordNotEmpty = false;
                binding.editTextConfirmPassword.setError("konfirmasi password harus sama");
            } else {
                isConfirmPasswordNotEmpty = true;
                strConfirmNewPassword = binding.editTextConfirmPassword.getText().toString().trim();
            }

            if ((isNewPasswordNotEmpty==true) && (isConfirmPasswordNotEmpty==true)){
                changePasswordAPI();
            }
        }
    }

    private void changePasswordAPI() {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        final Call<ResponseData> getData = service.changePassword(
                userID, strNewPassword);
        getData.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                if (response.isSuccessful() && response.body() !=null) {
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                    Intent intentLogin = new Intent(ActivityResetPassword.this, ActivityLogin.class);
                    startActivity(intentLogin);
                    finishAffinity();
                }else{
                    Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
}