package removed.developer.projectlaundry.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import removed.developer.projectlaundry.MapsActivity;
import removed.developer.projectlaundry.R;
import removed.developer.projectlaundry.models.DataName;

public class AdapterBranchToMaps extends RecyclerView.Adapter<AdapterBranchToMaps.ViewHolder> {

    private List<DataName> locationLaundrysArrayList;
    private Context context;

    public AdapterBranchToMaps(List<DataName> locationLaundrysArrayList, Context context) {
        this.locationLaundrysArrayList = locationLaundrysArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterBranchToMaps.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_location, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterBranchToMaps.ViewHolder holder, int position) {
        DataName locationLaundrys = locationLaundrysArrayList.get(position);
        holder.textViewLocation.setText(locationLaundrys.getName());
        holder.textViewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, MapsActivity.class);
                i.putExtra("branch", holder.textViewLocation.getText().toString().trim());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return locationLaundrysArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewLocation;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            textViewLocation = itemView.findViewById(R.id.textViewLocation);
        }
    }
}
