package removed.developer.projectlaundry.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import removed.developer.projectlaundry.DataClass.Historys;
import removed.developer.projectlaundry.R;
import removed.developer.projectlaundry.models.DataOrder;

public class AdapterHistoryLaundry extends RecyclerView.Adapter<AdapterHistoryLaundry.ViewHolder> {

    private List<DataOrder> historysArrayList;
    private Context context;

    public AdapterHistoryLaundry(List<DataOrder> historysArrayList, Context context) {
        this.historysArrayList = historysArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterHistoryLaundry.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_history_laundry, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterHistoryLaundry.ViewHolder holder, int position) {
        DataOrder historys = historysArrayList.get(position);
        String status = "";

        if (historys.getIsDelivered() == 1){
            status = "Sudah Selesai";
        }else if (historys.getIsProcessed() == 1){
            status = "Sedang Dicuci";
        }else if(historys.getIsPicked() == 1){
            status = "Sudah Diambil";
        }else if(historys.getIsAccepted() == 1){
            status = "Menunggu Jemput";
        }else{
            status = "Menunggu Respon";
        }

        if (historys.getIsDelivered() == 1 && historys.getIsPaid().equals(0)){
            status = "Dibatalin";
        }

        holder.textViewOrderDate.setText(historys.getPickupTime());
        holder.textViewStatus.setText(status);
        holder.textViewFinishDate.setText(historys.getFinishTime());
        holder.textViewBranch.setText(historys.getLaundryName());
        holder.textViewPackage.setText(historys.getPacketName());
        holder.textViewPrice.setText("Rp. " + historys.getPrice());

    }


    @Override
    public int getItemCount() {
        return historysArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewOrderDate, textViewStatus, textViewFinishDate, textViewBranch, textViewPackage, textViewPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewOrderDate = itemView.findViewById(R.id.textViewOrderDate);
            textViewStatus = itemView.findViewById(R.id.textViewStatus);
            textViewFinishDate = itemView.findViewById(R.id.textViewFinishDate);
            textViewBranch = itemView.findViewById(R.id.textViewBranch);
            textViewPackage = itemView.findViewById(R.id.textViewPackage);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
        }
    }

}
