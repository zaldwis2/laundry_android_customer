package removed.developer.projectlaundry.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import removed.developer.projectlaundry.DataClass.LocationLaundrys;
import removed.developer.projectlaundry.R;
import removed.developer.projectlaundry.models.DataName;

public class AdapterLocationLaundry extends RecyclerView.Adapter<AdapterLocationLaundry.ViewHolder> {

    private List<DataName> locationLaundrysArrayList;
    private Context context;
    private LocalBroadcastManager broadcaster;

    public AdapterLocationLaundry(List<DataName> locationLaundrysArrayList, Context context) {
        this.locationLaundrysArrayList = locationLaundrysArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterLocationLaundry.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_location, parent, false);
        broadcaster = LocalBroadcastManager.getInstance(context);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterLocationLaundry.ViewHolder holder, int position) {
        DataName locationLaundrys = locationLaundrysArrayList.get(position);
        holder.textViewLocation.setText(locationLaundrys.getName());
        holder.textViewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent("chooseLocation");
                i.putExtra("laundry_name",locationLaundrys.getName());
                i.putExtra("laundry_id",locationLaundrys.getId());
                broadcaster.sendBroadcast(i);
                Toast.makeText(context, holder.textViewLocation.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return locationLaundrysArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewLocation;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewLocation = itemView.findViewById(R.id.textViewLocation);

        }
    }
}
