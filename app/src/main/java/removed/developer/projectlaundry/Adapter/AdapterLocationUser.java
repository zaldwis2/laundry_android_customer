package removed.developer.projectlaundry.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import removed.developer.projectlaundry.DataClass.LocationUser;
import removed.developer.projectlaundry.R;
import removed.developer.projectlaundry.models.DataName;

public class AdapterLocationUser extends RecyclerView.Adapter<AdapterLocationUser.ViewHolder> {

    private List<DataName> locationUserArrayList = null;
    private Context context;
    private LocalBroadcastManager broadcaster;

    public AdapterLocationUser(List<DataName> locationUserArrayList, Context context) {
        this.context = context;
        this.locationUserArrayList = locationUserArrayList;
    }

    @NonNull
    @Override
    public AdapterLocationUser.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_location, parent, false);
        broadcaster = LocalBroadcastManager.getInstance(context);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterLocationUser.ViewHolder holder, int position) {
        DataName locationUser = locationUserArrayList.get(position);

        holder.textViewLocation.setText(locationUser.getName());
        holder.textViewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent("chooseLocationUser");
                i.putExtra("location_name", locationUser.getName());
                i.putExtra("location_id", locationUser.getId());
                broadcaster.sendBroadcast(i);
                Toast.makeText(context, holder.textViewLocation.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return locationUserArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewLocation;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewLocation = itemView.findViewById(R.id.textViewLocation);
        }
    }
}
