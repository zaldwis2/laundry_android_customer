package removed.developer.projectlaundry.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import removed.developer.projectlaundry.DataClass.LaundryServices;
import removed.developer.projectlaundry.R;
import removed.developer.projectlaundry.models.DataName;

public class AdapterServiceLaundry extends RecyclerView.Adapter<AdapterServiceLaundry.ViewHolder> {

    private List<DataName> laundryServicesArrayList = null;
    private Context context;
    private LocalBroadcastManager broadcastManager;

    public AdapterServiceLaundry(List<DataName> laundryServicesArrayList, Context context) {
        this.context = context;
        this.laundryServicesArrayList = laundryServicesArrayList;
    }

    @NonNull
    @Override
    public AdapterServiceLaundry.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_location, parent, false);
        broadcastManager = LocalBroadcastManager.getInstance(context);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterServiceLaundry.ViewHolder holder, int position) {
        DataName laundryServices = laundryServicesArrayList.get(position);

        holder.textViewService.setText(laundryServices.getName());
        holder.textViewService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent("chooseServiceLaundry");
                i.putExtra("service_name", laundryServices.getName());
                i.putExtra("service_id", laundryServices.getId());
                broadcastManager.sendBroadcast(i);
                Toast.makeText(context, holder.textViewService.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return laundryServicesArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewService;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewService = itemView.findViewById(R.id.textViewLocation);
        }
    }
}
