package removed.developer.projectlaundry.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import removed.developer.projectlaundry.DataClass.TypePackages;
import removed.developer.projectlaundry.R;
import removed.developer.projectlaundry.models.DataName;

public class AdapterTypePackage extends RecyclerView.Adapter<AdapterTypePackage.ViewHolder> {

    final Context context;
    List<DataName> typePackagesArrayList = null;
    private LocalBroadcastManager broadcaster;

    public AdapterTypePackage(List<DataName> typePackagesArrayList,Context context) {
        this.context = context;
        this.typePackagesArrayList = typePackagesArrayList;
    }

    @NonNull
    @Override
    public AdapterTypePackage.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_package, parent, false);
        broadcaster = LocalBroadcastManager.getInstance(context);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterTypePackage.ViewHolder holder, int position) {
        final DataName typePackages = typePackagesArrayList.get(position);
        holder.textViewPackage.setText(typePackages.getName());
        holder.textViewPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent("choosePackage");
                i.putExtra("packet_name",typePackages.getName());
                i.putExtra("packet_id",typePackages.getId());
                broadcaster.sendBroadcast(i);
                Toast.makeText(context, holder.textViewPackage.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return typePackagesArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewPackage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewPackage = itemView.findViewById(R.id.textViewPackage);
        }
    }
}
