package removed.developer.projectlaundry.DataClass;

public class Historys {

    private String orderDate;
    private String status;
    private String finishDate;
    private String branch;
    private String packageName;
    private String price;

    public Historys(String orderDate, String status, String finishDate, String branch, String packageName, String price) {
        this.orderDate = orderDate;
        this.status = status;
        this.finishDate = finishDate;
        this.branch = branch;
        this.packageName = packageName;
        this.price = price;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
