package removed.developer.projectlaundry.DataClass;

public class LocationLaundrys {

    String location;

    public LocationLaundrys(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
