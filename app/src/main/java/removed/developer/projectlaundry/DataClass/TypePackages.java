package removed.developer.projectlaundry.DataClass;

public class TypePackages {

    String namePackage, detailPackage;

    public TypePackages() {
    }

    public String getDetailPackage() {
        return detailPackage;
    }

    public void setDetailPackage(String detailPackage) {
        this.detailPackage = detailPackage;
    }

    public String getNamePackage() {
        return namePackage;
    }

    public void setNamePackage(String namePackage) {
        this.namePackage = namePackage;
    }
}
