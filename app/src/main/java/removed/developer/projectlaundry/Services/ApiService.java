package removed.developer.projectlaundry.Services;

import java.math.BigInteger;

import removed.developer.projectlaundry.models.ResponseData;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ApiService {

    @FormUrlEncoded
    @POST("register")
    Call<ResponseData> register(
            @Field("email") String email,
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("password") String password
    );

    @GET("login")
    Call<ResponseData> login(
            @Query("email") String email,
            @Query("password") String password
    );

    @GET("getUserData")
    Call<ResponseData> getUserData(
            @Query("id") Integer id
    );

    @FormUrlEncoded
    @PUT("updateUserData")
    Call<ResponseData> updateUserData(
            @Field("id") Integer id,
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone") String phone
    );

    @GET("forgotPassword")
    Call<ResponseData> forgotPassword(
            @Query("email") String email,
            @Query("name") String name,
            @Query("phone") String phone
    );

    @FormUrlEncoded
    @PUT("changePassword")
    Call<ResponseData> changePassword(
            @Field("id") Integer id,
            @Field("password") String password
    );

    @FormUrlEncoded
    @PUT("changePassword2")
    Call<ResponseData> changePassword2(
            @Field("id") Integer id,
            @Field("old_password") String old_password,
            @Field("new_password") String new_password
    );

    @FormUrlEncoded
    @POST("order")
    Call<ResponseData> order(
            @Field("customer_id") Integer customer_id,
            @Field("location_id") Integer location_id,
            @Field("laundry_id") Integer laundry_id,
            @Field("service_id") Integer service_id,
            @Field("packet_id") Integer packet_id,
            @Field("pickup_time") String pickup_time,
            @Field("finish_time") String finish_time
    );

    @GET("getOrder")
    Call<ResponseData> getOrder(
            @Query("customer_id") Integer customer_id
    );

    @GET("getOrders")
    Call<ResponseData> getOrders(
            @Query("customer_id") Integer customer_id
    );

    @GET("getLaundries")
    Call<ResponseData> getLaundries(
    );

    @GET("getPackets")
    Call<ResponseData> getPackets(
    );

    @GET("getLocations")
    Call<ResponseData> getLocations(
    );

    @GET("getServices")
    Call<ResponseData> getServices(
    );

    @FormUrlEncoded
    @PUT("cancelOrder")
    Call<ResponseData> cancelOrder(
            @Field("id") Integer id,
            @Field("is_accepted") Integer is_accepted,
            @Field("is_picked") Integer is_picked,
            @Field("is_processed") Integer is_processed,
            @Field("is_delivered") Integer is_delivered
    );

    @FormUrlEncoded
    @PUT("updateBalance")
    Call<ResponseData> updateBalance(
            @Field("id") Integer id,
            @Field("balance") Integer balance
    );

    @GET("getBalance")
    Call<ResponseData> getBalance(
            @Query("id") Integer customer_id
    );

    @FormUrlEncoded
    @PUT("payOrder")
    Call<ResponseData> payOrder(
            @Field("order_id") Integer order_id,
            @Field("customer_id") Integer customer_id
    );
}
