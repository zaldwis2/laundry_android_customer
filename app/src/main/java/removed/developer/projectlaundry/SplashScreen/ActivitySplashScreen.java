package removed.developer.projectlaundry.SplashScreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import removed.developer.projectlaundry.ActivityDashboard;
import removed.developer.projectlaundry.ActivityLogin;
import removed.developer.projectlaundry.databinding.ActivitySplashScreenBinding;
import removed.developer.projectlaundry.utility.SharedPreferenceManager;

public class ActivitySplashScreen extends AppCompatActivity {
    String userID;

    ActivitySplashScreenBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        userID = SharedPreferenceManager.getInstance(ActivitySplashScreen.this).getUserId();

        super.onCreate(savedInstanceState);
        binding = ActivitySplashScreenBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (userID.isEmpty()){
                    final Intent mainIntent = new Intent(getApplicationContext(), ActivityLogin.class);
                    startActivity(mainIntent);
                    finishAffinity();
                }else{
                    final Intent mainIntent = new Intent(getApplicationContext(), ActivityDashboard.class);
                    startActivity(mainIntent);
                    finishAffinity();
                }
            }
        }, 3000);


    }
}