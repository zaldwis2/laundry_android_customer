package removed.developer.projectlaundry.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;
import java.util.List;

public class ResponseData {
    @SerializedName("error")
    private Boolean error;

    @SerializedName("message")
    private String message;

    @SerializedName("balance")
    private Integer balance;

    @SerializedName("dataUser")
    private DataUser dataUser;


    @SerializedName("dataOrder")
    private DataOrder dataOrder;

    @SerializedName("dataOrders")
    private List<DataOrder> dataOrders = null;

    @SerializedName("dataServices")
    private List<DataName> dataServices = null;

    @SerializedName("dataLaundries")
    private List<DataName> dataLaundries = null;

    @SerializedName("dataPackets")
    private List<DataName> dataPackets = null;

    @SerializedName("dataLocations")
    private List<DataName> dataLocations = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataUser getDataUser() {
        return dataUser;
    }

    public void setDataUser(DataUser dataUser) {
        this.dataUser = dataUser;
    }

    public DataOrder getDataOrder() {
        return dataOrder;
    }

    public void setDataOrder(DataOrder dataOrder) {
        this.dataOrder = dataOrder;
    }

    public List<DataOrder> getDataOrders() {
        return dataOrders;
    }

    public void setDataOrders(List<DataOrder> dataOrders) {
        this.dataOrders = dataOrders;
    }

    public List<DataName> getDataServices() {
        return dataServices;
    }

    public void setDataServices(List<DataName> dataServices) {
        this.dataServices = dataServices;
    }

    public List<DataName> getDataLaundries() {
        return dataLaundries;
    }

    public void setDataLaundries(List<DataName> dataLaundries) {
        this.dataLaundries = dataLaundries;
    }

    public List<DataName> getDataPackets() {
        return dataPackets;
    }

    public void setDataPackets(List<DataName> dataPackets) {
        this.dataPackets = dataPackets;
    }

    public List<DataName> getDataLocations() {
        return dataLocations;
    }

    public void setDataLocations(List<DataName> dataLocations) {
        this.dataLocations = dataLocations;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}

